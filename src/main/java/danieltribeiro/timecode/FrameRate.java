package danieltribeiro.timecode;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Essa enumeração representa um base de tempo contendo frame rate (quadros por
 * segundo)
 */
public class FrameRate {

  static final FrameRate FRAME_RATE_23_98 = new FrameRate("23.98", 23.98, true, ":");
  static final FrameRate FRAME_RATE_24_00 = new FrameRate("24.00", 24.00, false, ":");
  static final FrameRate FRAME_RATE_25_00 = new FrameRate("25.00", 25.00, false, ":");
  static final FrameRate FRAME_RATE_29_97 = new FrameRate("29.97", 29.97, true, ":");
  static final FrameRate FRAME_RATE_30_00 = new FrameRate("30.00", 30.00, false, ":");
  static final FrameRate FRAME_RATE_59_94 = new FrameRate("59.94", 59.94, true, ":");
  static final FrameRate FRAME_RATE_60_00 = new FrameRate("60.00", 60.00, false, ":");

  /**
   * Hora em milisegundos
   */
  public static final int ONE_HOUR_IN_MILLIS = 3600000;

  /**
   * Dia em milisegundos
   */
  public static final int ONE_DAY_IN_MILLIS = 86400000;

  /**
   * Lista de unidades de medida de tempo: milisegundos, string e numero de frames
   */
  public static enum MesureUnit {
    MILLISECONDS, STRING, FRAME_NUMBER, COMPONENTS, TIMECODE_OBJECT
  };

  /**
   * Nome da base de tempo
   */
  private String name;

  /**
   * Taxa de frames da base de tempo
   */
  private Double value;

  /**
   * Assume true caso haja dropFrame
   */
  private boolean dropFrame;

  /*
  * Separador da parte frames na string
  */
  private String frameSeparator;


  public FrameRate(String name, Double value, boolean dropFrame, String frameSeparator) {
    this.name = name;
    this.value = value;
    this.dropFrame = dropFrame;
    this.frameSeparator = frameSeparator;
  }

  /**
   * @return Um Array com os Frame Rates
   */public static FrameRate[] getFrameRates() {
    return new FrameRate[] {
      FRAME_RATE_23_98,
      FRAME_RATE_24_00,
      FRAME_RATE_25_00,
      FRAME_RATE_29_97,
      FRAME_RATE_30_00,
      FRAME_RATE_59_94,
      FRAME_RATE_60_00
    };
  }

  /**
   * @param name O nome do Frame rate
   * @return FrameRate O objeto de Frame Rate representado por esse nome
   */
  public static FrameRate getFrameRate(String name) {
    for (FrameRate tb : getFrameRates()) {
      if (tb.name.equals(name)) {
        return tb;
      }
    }
    throw new IllegalArgumentException("Timebase nao encontrado: " + name);
  }

  /**
   * @return the frameSeparator
   */
  public String getFrameSeparator() {
    return frameSeparator;
  }

  /**
   * @param frameSeparator the frameSeparator to set
   */
  public void setFrameSeparator(String frameSeparator) {
    this.frameSeparator = frameSeparator;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the value
   */
  public Double getValue() {
    return value;
  }

  /**
   * @param value the value to set
   */
  public void setValue(Double value) {
    this.value = value;
  }

  /**
   * @return the dropFrame
   */
  public boolean isDropFrame() {
    return dropFrame;
  }

  /**
   * @param dropFrame the dropFrame to set
   */
  public void setDropFrame(boolean dropFrame) {
    this.dropFrame = dropFrame;
  }

  public String toString() {
    return name;
  }

  /**
   * Retorna o frame rate arredondado para o inteiro mais proximo.<br/>
   * p.ex. 29.97 vira 30
   */
  public int getRoundedFrameRate() {
    return (int) Math.round((double)value);
  }

  /**
   * Converte de numero de frames para milisegundos
   * 
   * @param fn Numero de Frames
   */
  public int millisecondsFromFrameNumber(int fn) {
    return (int) Math.floor(((double) fn / (double) value) * 1000);
  }

  /**
   * Converte de milisegundos para numero de frames
   * 
   * @param millis Milisegundos
   */
  public int frameNumberFromMilliseconds(int millis) {
    return (int) Math.ceil(((double)millis / 1000.0) * (double)value);
  }

  /**
   * Converte para string o número de frames
   * 
   * @param frameNumber        Numero de frames
   */
  public String stringFromFrameNumber(int frameNumber) {
    int[] c = componentsFromFrameNumber(frameNumber);
    return stringFromComponents(c);
  }

  /**
   * Converte de string para número de frames
   * 
   * @param s string a ser convertida
   */
  public int frameNumberFromString(String s) {
    return frameNumberFromComponents(componentsFromString(s));
  }

  /**
   * Converte de milisegundos para string
   * 
   * @param milis          Inteiro com o valor dos milisegundos
   * @param frameSeparetor caracter separador dos campos da string
   */
  public String stringFromMilliseconds(int millis) {
    int[] c = componentsFromMilliseconds(millis);
    return stringFromComponents(c);
  }

  /**
   * Converte de string para milisegundos
   * 
   * @param s string a ser convertida
   */
  public int millisecondsFromString(String s) {
    return millisecondsFromComponents(componentsFromString(s));
  }
  /**
   * Converte de componentes para string
   * 
   * @param components         objeto componente
   */
  public String stringFromComponents(int[] components) {
    java.text.DecimalFormat f = new java.text.DecimalFormat("00");
    int[] c = components;
    return f.format(c[0]) + ":" + f.format(c[1]) + ":" + f.format(c[2]) + frameSeparator + f.format(c[3]);
  }

  /**
   * Converte string para componente
   * 
   * @param s String a ser convertida
   */
  public int[] componentsFromString(String s) {
    String[] splitted = s.split("[\\.\\:\\;]");
    List<String> list = Arrays.asList((String[]) splitted);
    List<Integer> integers = list.stream().map(s2 -> Integer.valueOf(s2)).collect(Collectors.toList());
    return integers.stream().mapToInt(i -> i).toArray();
  }

  /**
   * Converte de milisegundos para componente
   * 
   * @param millis milisegundos
   * @return Retorna o componente
   */
  public int[] componentsFromMilliseconds(int millis) {
    int frameNumber = frameNumberFromMilliseconds(millis);
    return componentsFromFrameNumber(frameNumber);
  }

  /**
   * Converte de componentes para milisegundos
   * 
   * @param c objeto componente
   * @return Valor em milisegundos
   */
  public int millisecondsFromComponents(int[] c) {
    return millisecondsFromFrameNumber(frameNumberFromComponents(c));
  }

  /**
   * Converte de componente para número de frames
   * 
   * @param c Objeto componente
   * @return retorna número de frames
   */
  public int frameNumberFromComponents(int[] c) {
    int hours = c[0];
    int minutes = c[1];
    int seconds = c[2];
    int frames = c[3];

    int roundedFrameRate = getRoundedFrameRate();

    int frameNumber = (3600 * roundedFrameRate) * hours + (60 * roundedFrameRate) * minutes + roundedFrameRate * seconds
        + frames;

    if (dropFrame) {
      int droppedFrames = calculateFramesToDrop(c);
      frameNumber -= droppedFrames;
    }

    return frameNumber;
  }

  /**
   * Obtem um array com [hours, minutes, seconds, frames]
   */
  public int[] componentsFromFrameNumber(int frameNumber) {
    boolean df = dropFrame;

    if (df) {
      int droppedFrames = calculateFramesToAdd(frameNumber);
      frameNumber += droppedFrames;
    }

    int roundedFrameRate = (int) Math.round(value);

    int frames = frameNumber % roundedFrameRate;
    int seconds = (int) Math.floor(frameNumber / (roundedFrameRate)) % 60;
    int minutes = (int) Math.floor((frameNumber / (roundedFrameRate)) / 60) % 60;
    int hours = (int) Math.floor(((frameNumber / (roundedFrameRate)) / 60) / 60) % roundedFrameRate;
    int[] tc = { hours, minutes, seconds, frames };
    return tc;
  }

  public Timecode timecodeFromFrameNumber(int frameNumber) {
    String string = stringFromFrameNumber(frameNumber);
    int millis = millisecondsFromFrameNumber(frameNumber);
    int[] components = componentsFromFrameNumber(frameNumber);
    return new TimecodeImpl(string, frameNumber, millis, components, this);
  }

  public Timecode timecodeFromMilliseconds(int millis) {
    String string = stringFromMilliseconds(millis);
    int frameNumber = frameNumberFromMilliseconds(millis);
    int[] components = componentsFromMilliseconds(millis);
    return new TimecodeImpl(string, frameNumber, millis, components, this);
  }

  public Timecode timecodeFromString(String string) {
    int millis =  millisecondsFromString(string);
    int frameNumber = frameNumberFromString(string);
    int[] components = componentsFromString(string);
    return new TimecodeImpl(string, frameNumber, millis, components, this);
  }

  public Timecode timecodeFromComponents(int[] components) {
    int millis =  frameNumberFromComponents(components);
    int frameNumber = frameNumberFromComponents(components);
    String string = stringFromComponents(components);
    return new TimecodeImpl(string, frameNumber, millis, components, this);
  }

  public int calculateFramesToAdd(long frameNumber) {
    boolean df = dropFrame;

    int droppedFrames = 0;

    // drop frame para 29.97
    if (df && (value > 29.00 && value < 30.00)) {
      int D = (int) (frameNumber / 17982);
      int M = (int) (frameNumber % 17982);
      droppedFrames = (18 * D) + 2 * ((M - 2) / 1798);
    }

    // drop frame para 59.94
    if (df && (value > 59.00 && value < 60.00)) {
      int D = (int) (frameNumber / 35964);
      int M = (int) (frameNumber % 35964);
      droppedFrames = (36 * D) + 4 * ((M - 4) / 3596);
    }

        
    // dropFrame para 23.98
    if (df && (value > 23.00 && value < 24.00)) {
      droppedFrames = (int) (frameNumber * 0.0008340284);
    }
    
    return droppedFrames;
  }

  /**
   * Retorna número de minutos exeto os dos 10 últimos frames
   * 
   * @param c Array com hora e minuto
   * @return Número de minutos exeto os dos 10 últimos frames
   */
  private int calculateMinutesExcept10(int[] c) {
    return calculateMinutesExcept10(c[0], c[1]);
  }

  /**
   * Calcula número de minutos exeto os dos 10 últimos frames
   * 
   * @param hours   Valor inteiro do número de horas
   * @param minutes Valor inteiro do número de minutos
   * @return totalMinutesExcept10 - Número de minutos exeto os dos 10 últimos
   *         frames
   */
  private int calculateMinutesExcept10(int hours, int minutes) {
    int totalMinutes = 60 * hours + minutes;
    int minutesDiv10 = (int) (totalMinutes / 10);
    int totalMinutesExcept10 = (totalMinutes - minutesDiv10);
    return totalMinutesExcept10;
  }

  private int calculateFramesToDrop(int[] tc) {
    int framesToDrop = 0;
    if (value > 23 && value < 24 && dropFrame) {
      int frameNumber = (((((tc[0] * 60) + tc[1]) * 60) + tc[2]) * 24) + tc[3];
      framesToDrop = (int) (frameNumber / (1200));
    } else if (value > 29 && value < 30 && dropFrame) {
      framesToDrop = 2 * calculateMinutesExcept10(tc);
    } else if (value > 59 && value < 60 && dropFrame) {
      framesToDrop = 4 * calculateMinutesExcept10(tc);
    }
    return framesToDrop;
  }


  /**
   * Converte entre unidades
   * 
   * @param value valor a ser convertido
   * @param from  Unidade de origem
   * @param to    unidade de destino
   * @return Valor convertido
   */
  public Object convertUnit(Object value, MesureUnit from, MesureUnit to) {
    if (from == to) {
      return value;
    }
    Object val;

    if (from == MesureUnit.STRING) {
      String v2 = (String) value;
      switch (to) {
      case FRAME_NUMBER:
        val = this.frameNumberFromString(v2);
        break;
      case MILLISECONDS:
        val = this.millisecondsFromString(v2);
        break;
      case COMPONENTS:
        val = this.componentsFromString(v2);
        break;
      case TIMECODE_OBJECT:
        val = this.timecodeFromString(v2);
        break;
      default:
        throw new IllegalArgumentException("Invalid unit to convert: " + to);
      }
    } else if (from == MesureUnit.FRAME_NUMBER) {
      Integer v2 = (Integer) value;
      switch (to) {
      case STRING:
        val = this.stringFromFrameNumber(v2);
        break;
      case MILLISECONDS:
        val = this.millisecondsFromFrameNumber(v2);
        break;
      case COMPONENTS:
        val = this.componentsFromFrameNumber(v2);
        break;
      case TIMECODE_OBJECT:
        val = this.timecodeFromFrameNumber(v2);
        break;
      default:
        throw new IllegalArgumentException("Invalid unit to convert: " + to);
      }
    } else if (from == MesureUnit.COMPONENTS) {
      int[] v2 = (int[]) value;
      switch (to) {
      case STRING:
        val = this.stringFromComponents(v2);
        break;
      case MILLISECONDS:
        val = this.millisecondsFromComponents(v2);
        break;
      case FRAME_NUMBER:
        val = this.frameNumberFromComponents(v2);
        break;
        case TIMECODE_OBJECT:
          val = this.timecodeFromComponents(v2);
          break;
      default:
        throw new IllegalArgumentException("Invalid unit to convert: " + to);
      }
    } else if (from == MesureUnit.MILLISECONDS) {
      Integer v2 = (Integer) value;
      switch (to) {
      case STRING:
        val = this.stringFromMilliseconds(v2);
        break;
      case COMPONENTS:
        val = this.componentsFromMilliseconds(v2);
        break;
        case FRAME_NUMBER:
        val = this.frameNumberFromMilliseconds(v2);
        break;
        case TIMECODE_OBJECT:
          val = this.timecodeFromMilliseconds(v2);
          break;
      default:
        throw new IllegalArgumentException("Invalid convertion unit to: " + to);
      }
    } else {
      throw new IllegalArgumentException("Invalid convertion unit from: " + from);
    }

    return val;
  }

  public Object convertFrameRate(Object value, MesureUnit unit, FrameRate to) {
    switch(unit) {
      case STRING:
        return this.convertFrameRate(value, unit, to, "00:00:00:00");
      case COMPONENTS:
        return this.convertFrameRate(value, unit, to, new int[] {0, 0, 0, 0});
      case MILLISECONDS:
      case FRAME_NUMBER:
        return this.convertFrameRate(value, unit, to, 0);
      default:
        throw new IllegalArgumentException("unrecognized unit: " + unit);
    }
  }

  /**
  * Converte o Frame Rate entre a base de tempo corrente e a base de tempo destino (to)
  * @param value O valor a ser convertido
  * @param unit A unidade de medida
  * @param to A base de tempo final da conversão
  * @param baseValue O valor base para começar a contar os drop frames do timecode
  * @return Object a unidade de medida na base de tempo final da conversão
  */
  public Object convertFrameRate(Object value, MesureUnit unit, FrameRate to, Object baseValue) {
    int srcMillis;
    int baseMillis;
    
    if (unit == MesureUnit.STRING) {
      String v2 = (String) value;
      srcMillis = this.millisecondsFromString(v2);
      baseMillis = this.millisecondsFromString((String) baseValue);
    } else if (unit == MesureUnit.FRAME_NUMBER) {
      Integer v2 = (Integer) value;
      srcMillis = this.millisecondsFromFrameNumber(v2);
      baseMillis = this.millisecondsFromFrameNumber((Integer) baseValue);
    } else if (unit == MesureUnit.COMPONENTS) {
      int[] v2 = (int[]) value;
      srcMillis = this.millisecondsFromComponents(v2);
      baseMillis = this.millisecondsFromComponents((int[]) baseValue);
    } else if (unit == MesureUnit.MILLISECONDS) {
      srcMillis = (int) value;
      baseMillis = (int) baseValue;
    } else {
      throw new IllegalArgumentException("Invalid convertion unit from: " + unit);
    }

    int adjust = 0;
    if (this.getName() == "30.00" && to.getName() == "29.97") {
      adjust = (int) ((srcMillis - baseMillis) * 0.001);
    } else if (this.getName() == "29.97" && to.getName() == "30.00") {
      adjust = (int) ((srcMillis - baseMillis) * -0.001);
    } else if (this.getName() == "23.98" && to.getName() == "29.97") {
      adjust = (int) ((srcMillis - baseMillis) * 0.001);
    } else if (this.getName() == "29.97" && to.getName() == "23.98") {
      adjust = (int) ((srcMillis - baseMillis) * -0.001);
    } else if (this.getName() == "29.97" && to.getName() == "25.00") {
      adjust = (int) ((srcMillis - baseMillis) * -0.040958);
    } else if (this.getName() == "25.00" && to.getName() == "29.97") {
      adjust = (int) ((srcMillis - baseMillis) * 0.042709);
    } else if (this.getName() == "25.00" && to.getName() == "30.00") {
      adjust = (int) ((srcMillis - baseMillis) * 0.041666);
    } else if (this.getName() == "30.00" && to.getName() == "25.00") {
      adjust = (int) ((srcMillis - baseMillis) * -0.04);
    }
    
    int toMilis = srcMillis + adjust;
    return to.convertUnit(toMilis, MesureUnit.MILLISECONDS, unit);
  }

  public Timecode convertToTimecode(Object value, MesureUnit unit) {
    if (unit == MesureUnit.STRING) {
      return timecodeFromString(value.toString());
    } else if (unit == MesureUnit.FRAME_NUMBER) {
      return timecodeFromFrameNumber((int)value);
    } else if (unit == MesureUnit.COMPONENTS) {
      return timecodeFromComponents((int[])value);
    } else if (unit == MesureUnit.MILLISECONDS) {
      return timecodeFromMilliseconds((int)value);
    } else {
      throw new IllegalArgumentException("Invalid convertion unit from: " + unit);
    }
  }
}