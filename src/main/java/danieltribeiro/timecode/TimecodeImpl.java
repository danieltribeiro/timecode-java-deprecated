package danieltribeiro.timecode;

public class TimecodeImpl implements Timecode {
  private String string;
  private int frameNumber;
  private int milliseconds;
  private int[] components;
  private FrameRate frameRate;
  
  

  public TimecodeImpl(String string, int frameNumber, int milliseconds, int[] components, FrameRate frameRate) {
    this.string = string;
    this.frameNumber = frameNumber;
    this.milliseconds = milliseconds;
    this.components = components;
    this.frameRate = frameRate;
  }

  public TimecodeImpl() {
  }


  @Override
  public String getString() {
    return string;
  }  
  /**
  * @param string the string to set
  */
  public void setString(String string) {
    this.string = string;
  }
  
  @Override
  public int getFrameNumber() {
    return frameNumber;
  }
  
  /**
  * @param frameNumber the frameNumber to set
  */
  public void setFrameNumber(int frameNumber) {
    this.frameNumber = frameNumber;
  }
  

  @Override
  public int getMilliseconds() {
    return milliseconds;
  }

  /**
  * @param milliseconds the milliseconds to set
  */
  public void setMilliseconds(int milliseconds) {
    this.milliseconds = milliseconds;
  }
  
  @Override
  public int[] getComponents() {
    return components;
  }

  /**
  * @param components the components to set
  */
  public void setComponents(int[] components) {
    this.components = components;
  }

  @Override
  public FrameRate getFrameRate() {
    return frameRate;
  }

  /**
   * @param frameRate the frameRate to set
   */
  public void setFrameRate(FrameRate frameRate) {
    this.frameRate = frameRate;
  }

  @Override
  public String toString() {
    return string + "@" + frameRate + " | " + frameNumber + " frames | " + milliseconds + " milis";
  }

}