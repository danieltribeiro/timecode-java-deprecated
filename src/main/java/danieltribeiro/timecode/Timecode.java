package danieltribeiro.timecode;

/**
 * Classe que representa um instante no tempo em forma de timecode
 * Tem uma base de tempo (Frame Rate) associada.
 */
interface Timecode {
  /**
   * @return A String que representa esse timecode
   */
  public String getString();
  
  /**
   * 
   * @return o Numero do frame desse timecode
   */
  public int getFrameNumber();


  /**
   * 
   * @return O milisegundo que representa esse timecode
   */
  public int getMilliseconds();

  /**
   * 
   * @return Um array de int contendo os componentes desse timecode na ordem [hora, minuto, segundo, frame]. P.ex. "01:02:03:04" é representado em componentes como [1, 2, 3, 4]
   */
  public int[] getComponents();

  /**
   * 
   * @return O frameRate associado a esse timecode
   */
  FrameRate getFrameRate();
}