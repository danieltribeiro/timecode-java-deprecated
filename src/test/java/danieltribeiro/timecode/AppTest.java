/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package danieltribeiro.timecode;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AppTest {
  @Test public void testSimpleConversion() {
    Timecode val = new App().convert(new String[] {"30.00", "01:00:00:00", "29.97"});
    assertEquals("01:00:03:18", val.getString());
  }

  @Test public void testConversionWithBaseValue() {
    Timecode val = new App().convert(new String[] {"30.00", "02:00:00:00", "29.97", "01:00:00:00"});
    assertEquals("02:00:03:18", val.getString());
  }
}
