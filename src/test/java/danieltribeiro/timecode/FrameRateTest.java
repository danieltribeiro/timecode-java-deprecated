package danieltribeiro.timecode;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import danieltribeiro.timecode.FrameRate.MesureUnit;

public class FrameRateTest {

  /* Cada linha nos arrays representa um mesmo valor sob as diferentes representações. 
  * As colunas são (na ordem):
  *     String, Frame Number, Componentes, Milliseconds
  */
  


  private Object[][] timecodes6000 = {
    {"00:00:00:00", 0, new int[] {0, 0, 0, 0}, 0},
    {"00:00:00:01", 1, new int[] {0, 0, 0, 1}, 16},
    {"00:00:00:02", 2, new int[] {0, 0, 0, 2}, 32},
    {"00:00:00:10", 10, new int[] {0, 0, 0, 10}, 166},
    {"00:00:00:27", 27, new int[] {0, 0, 0, 27}, 450},
    {"00:00:00:58", 58, new int[] {0, 0, 0, 58}, 966},
    {"00:00:00:59", 59, new int[] {0, 0, 0, 59}, 982},
    {"00:00:01:00", 60, new int[] {0, 0, 1, 00}, 1000},
    {"00:00:59:59", 3599, new int[] {0, 0, 59, 59}, 59983},
    {"00:01:00:00", 3600, new int[] {0, 1, 0, 0}, 60000},
    {"00:01:00:01", 3601, new int[] {0, 1, 0, 1}, 60016},
    {"01:00:00:00", 216000, new int[] {1, 0, 0, 0}, 3600000},
    {"01:00:00:01", 216001, new int[] {1, 0, 0, 1}, 3600016},
    {"01:00:00:02", 216002, new int[] {1, 0, 0, 2}, 3600032},
  };


  @Test
  public void convertUnitTest6000() {
    FrameRate frameRate = FrameRate.getFrameRate("60.00");
    convertUnits(frameRate, timecodes6000);
  }

  
  private Object[][] timecodes5994 = {
    {"00:00:00:00", 0, new int[] {0, 0, 0, 0}, 0},
    {"00:00:00:01", 1, new int[] {0, 0, 0, 1}, 16},
    {"00:00:00:02", 2, new int[] {0, 0, 0, 2}, 32},
    {"00:00:00:10", 10, new int[] {0, 0, 0, 10}, 166},
    {"00:00:00:27", 27, new int[] {0, 0, 0, 27}, 450},
    {"00:00:00:58", 58, new int[] {0, 0, 0, 58}, 966},
    {"00:00:00:59", 59, new int[] {0, 0, 0, 59}, 984},
    {"00:00:01:00", 60, new int[] {0, 0, 1, 00}, 1000},
    {"00:00:59:59", 3599, new int[] {0, 0, 59, 59}, 60043},
    {"00:01:00:04", 3600, new int[] {0, 1, 0, 4}, 60060},
    {"00:01:00:05", 3601, new int[] {0, 1, 0, 5}, 60076},
    {"00:10:00:00", 35964, new int[] {0, 10, 0, 0}, 600000},
    {"01:00:00:00", 215784, new int[] {1, 0, 0, 0}, 3600000},
    {"01:00:00:01", 215785, new int[] {1, 0, 0, 1}, 3600016},
    {"01:00:00:02", 215786, new int[] {1, 0, 0, 2}, 3600032},
  };

  @Test
  public void convertUnitTest5994() {
    FrameRate frameRate = FrameRate.getFrameRate("59.94");
    convertUnits(frameRate, timecodes5994);
  }

  
  private Object[][] timecodes3000 = {
    {"00:00:00:00", 0, new int[] {0, 0, 0, 0}, 0},
    {"00:00:00:01", 1, new int[] {0, 0, 0, 1}, 33},
    {"00:00:00:02", 2, new int[] {0, 0, 0, 2}, 66},
    {"00:00:00:10", 10, new int[] {0, 0, 0, 10}, 333},
    {"00:00:00:27", 27, new int[] {0, 0, 0, 27}, 900},
    {"00:00:00:28", 28, new int[] {0, 0, 0, 28}, 933},
    {"00:00:00:29", 29, new int[] {0, 0, 0, 29}, 966},
    {"00:00:01:00", 30, new int[] {0, 0, 1, 00}, 1000},
    {"00:00:59:29", 1799, new int[] {0, 0, 59, 29}, 59966},
    {"00:01:00:00", 1800, new int[] {0, 1, 0, 0}, 60000},
    {"00:01:00:01", 1801, new int[] {0, 1, 0, 1}, 60033},
    {"00:01:00:02", 1802, new int[] {0, 1, 0, 2}, 60066},
    {"01:00:00:00", 108000, new int[] {1, 0, 0, 0}, 3600000},
    {"01:00:00:01", 108001, new int[] {1, 0, 0, 1}, 3600033},
    {"01:00:00:02", 108002, new int[] {1, 0, 0, 2}, 3600066},
  };
  
  @Test
  public void convertUnitTest3000() {
    FrameRate frameRate = FrameRate.getFrameRate("30.00");
    convertUnits(frameRate, timecodes3000);
  }

  
  private Object[][] timecodes2997 = {
    {"00:00:00:00", 0, new int[] {0, 0, 0, 0}, 0},
    {"00:00:00:01", 1, new int[] {0, 0, 0, 1}, 33},
    {"00:00:00:02", 2, new int[] {0, 0, 0, 2}, 66},
    {"00:00:00:10", 10, new int[] {0, 0, 0, 10}, 333},
    {"00:00:00:27", 27, new int[] {0, 0, 0, 27}, 900},
    {"00:00:00:28", 28, new int[] {0, 0, 0, 28}, 933},
    {"00:00:00:29", 29, new int[] {0, 0, 0, 29}, 967},
    {"00:00:01:00", 30, new int[] {0, 0, 1, 00}, 1001},
    {"00:00:59:29", 1799, new int[] {0, 0, 59, 29}, 60026},
    {"00:01:00:02", 1800, new int[] {0, 1, 0, 2}, 60060},
    {"00:01:00:03", 1801, new int[] {0, 1, 0, 3}, 60093},
    {"00:10:00:00", 17982, new int[] {0, 10, 0, 00}, 600000},
    {"01:00:00:00", 107892, new int[] {1, 0, 0, 0}, 3600000},
    {"01:00:00:01", 107893, new int[] {1, 0, 0, 1}, 3600033},
    {"01:00:00:02", 107894, new int[] {1, 0, 0, 2}, 3600066}
  };


  @Test
  public void convertUnitTest2997() {
    FrameRate frameRate = FrameRate.getFrameRate("29.97");
    convertUnits(frameRate, timecodes2997);
    
  }

  @Test
  public void assertOnlyValid2997() {
    FrameRate frameRate = FrameRate.getFrameRate("29.97");
    Object m = frameRate.convertUnit("01:01:00:00", MesureUnit.STRING, MesureUnit.MILLISECONDS);
    assertEquals("01:00:59:28", frameRate.convertUnit(m, MesureUnit.MILLISECONDS, MesureUnit.STRING));
  }

  private Object[][] timecodes2500 = {
    {"00:00:00:00", 0, new int[] {0, 0, 0, 0}, 0},
    {"00:00:00:01", 1, new int[] {0, 0, 0, 1}, 40},
    {"00:00:00:02", 2, new int[] {0, 0, 0, 2}, 80},
    {"00:00:00:10", 10, new int[] {0, 0, 0, 10}, 400},
    {"00:00:00:22", 22, new int[] {0, 0, 0, 22}, 880},
    {"00:00:00:23", 23, new int[] {0, 0, 0, 23}, 920},
    {"00:00:00:24", 24, new int[] {0, 0, 0, 24}, 960},
    {"00:00:01:00", 25, new int[] {0, 0, 1, 00}, 1000},
    {"00:00:59:24", 1499, new int[] {0, 0, 59, 24}, 59960},
    {"00:01:00:00", 1500, new int[] {0, 1, 0, 0}, 60000},
    {"00:01:00:01", 1501, new int[] {0, 1, 0, 1}, 60040},
    {"01:00:00:00", 90000, new int[] {1, 0, 0, 0}, 3600000},
    {"01:00:00:01", 90001, new int[] {1, 0, 0, 1}, 3600040},
    {"01:00:00:02", 90002, new int[] {1, 0, 0, 2}, 3600080},
  };

  @Test
  public void convertUnitTest2500() {
    FrameRate frameRate = FrameRate.getFrameRate("25.00");
    convertUnits(frameRate, timecodes2500);
  }

  

  private Object[][] timecodes2400 = {
    {"00:00:00:00", 0, new int[] {0, 0, 0, 0}, 0},
    {"00:00:00:01", 1, new int[] {0, 0, 0, 1}, 41},
    {"00:00:00:02", 2, new int[] {0, 0, 0, 2}, 82},
    {"00:00:00:10", 10, new int[] {0, 0, 0, 10}, 416},
    {"00:00:00:22", 22, new int[] {0, 0, 0, 22}, 916},
    {"00:00:00:23", 23, new int[] {0, 0, 0, 23}, 958},
    {"00:00:01:00", 24, new int[] {0, 0, 1, 00}, 1000},
    {"00:00:59:23", 1439, new int[] {0, 0, 59, 23}, 59958},
    {"00:01:00:00", 1440, new int[] {0, 1, 0, 0}, 60000},
    {"00:01:00:01", 1441, new int[] {0, 1, 0, 1}, 60041},
    {"01:00:00:00", 86400, new int[] {1, 0, 0, 0}, 3600000},
    {"01:00:00:01", 86401, new int[] {1, 0, 0, 1}, 3600041},
    {"01:00:00:02", 86402, new int[] {1, 0, 0, 2}, 3600082},
  };


  @Test
  public void convertUnitTest2400() {
    FrameRate frameRate = FrameRate.getFrameRate("24.00");
    convertUnits(frameRate, timecodes2400);
  }

  
  private Object[][] timecodes2398 = {
    {"00:00:00:00", 0, new int[] {0, 0, 0, 0}, 0},
    {"00:00:00:01", 1, new int[] {0, 0, 0, 1}, 41},
    {"00:00:00:02", 2, new int[] {0, 0, 0, 2}, 82},
    {"00:00:00:10", 10, new int[] {0, 0, 0, 10}, 416},
    {"00:00:00:22", 22, new int[] {0, 0, 0, 22}, 916},
    {"00:00:00:23", 23, new int[] {0, 0, 0, 23}, 958},
    {"00:00:01:00", 24, new int[] {0, 0, 1, 00}, 1000},
    {"00:00:59:23", 1438, new int[] {0, 0, 59, 23}, 59966},
    {"00:01:00:00", 1439, new int[] {0, 1, 0, 0}, 60008},
    {"00:01:00:01", 1440, new int[] {0, 1, 0, 1}, 60050},
    {"01:00:00:00", 86328, new int[] {1, 0, 0, 0}, 3600000},
    {"01:00:00:01", 86329, new int[] {1, 0, 0, 1}, 3600041},
    {"01:00:00:02", 86330, new int[] {1, 0, 0, 2}, 3600083},
  };

  @Test
  public void convertUnitTest2398() {
    FrameRate frameRate = FrameRate.getFrameRate("23.98");
    convertUnits(frameRate, timecodes2398);
  }


  @Test
  public void convertFrameRate3000To2997() {
    FrameRate frameRate3000 = FrameRate.getFrameRate("30.00");
    FrameRate frameRate2997 = FrameRate.getFrameRate("29.97");
    Object value = "01:00:00:00";
    assertEquals("Erro convertendo de 30.00 para 29.97 " + value, "01:00:03:18", frameRate3000.convertFrameRate(value, MesureUnit.STRING, frameRate2997));
  }

  
  @Test
  public void convertFrameRate2997to3000() {
    FrameRate frameRate3000 = FrameRate.getFrameRate("30.00");
    FrameRate frameRate2997 = FrameRate.getFrameRate("29.97");
    Object value = "01:00:03:17";
    assertEquals("Erro convertendo de 29.97 para 30.00 " + value, "01:00:00:00", frameRate2997.convertFrameRate(value, MesureUnit.STRING, frameRate3000));
  }

  @Test
  public void convertFrameRate2500to3000() {
    FrameRate frameRate3000 = FrameRate.getFrameRate("30.00");
    FrameRate frameRate2500 = FrameRate.getFrameRate("25.00");
    Object value = "01:00:00;00";
    Object expected = "01:02:30:00";
    assertEquals("Erro convertendo de 25.00 para 30.00 " + value, expected, frameRate2500.convertFrameRate(value, MesureUnit.STRING, frameRate3000));
  }

  @Test
  public void convertFrameRate3000to2500() {
    FrameRate frameRate3000 = FrameRate.getFrameRate("30.00");
    FrameRate frameRate2500 = FrameRate.getFrameRate("25.00");
    Object value = "01:00:00;00";
    Object expected = "00:57:36:00";
    assertEquals("Erro convertendo de 30.00 para 25.00 " + value, expected, frameRate3000.convertFrameRate(value, MesureUnit.STRING, frameRate2500));
  }

  @Test
  public void convertFrameRate2997to2500() {
    String srcFr = "29.97";
    String destFr = "25.00";
    FrameRate frameRate2997 = FrameRate.getFrameRate("29.97");
    FrameRate frameRate2500 = FrameRate.getFrameRate("25.00");
    Object value = "01:00:00;00";
    Object expected = "00:57:32:14";
    assertEquals("Erro convertendo de " + srcFr + " para " + destFr + " " + value, expected, frameRate2997.convertFrameRate(value, MesureUnit.STRING, frameRate2500));
  }

  @Test
  public void convertFrameRate2500to2997() {
    String srcFr = "25.00";
    String destFr = "29.97";
    FrameRate frameRate2997 = FrameRate.getFrameRate(srcFr);
    FrameRate frameRate2500 = FrameRate.getFrameRate(destFr);
    Object value = "01:00:00:00";
    Object expected = "01:02:33:22";
    assertEquals("Erro convertendo de " + srcFr + " para " + destFr + " " + value, expected, frameRate2997.convertFrameRate(value, MesureUnit.STRING, frameRate2500));
    value = "02:00:00:00";
    expected = "02:05:07:16";
    assertEquals("Erro convertendo de " + srcFr + " para " + destFr + " " + value, expected, frameRate2997.convertFrameRate(value, MesureUnit.STRING, frameRate2500));
    value = "03:00:00:00";
    expected = "03:05:07:16";
    Object baseValue = "01:00:00:00";
    assertEquals("Erro convertendo de " + srcFr + " para " + destFr + " " + value + " com valor base de " + baseValue, expected, frameRate2997.convertFrameRate(value, MesureUnit.STRING, frameRate2500, baseValue));
  }

  @Test
  public void convertFrameRate2398to2997() {
    String srcFr = "23.98";
    String destFr = "29.97";
    FrameRate srcFrameRate = FrameRate.getFrameRate(srcFr);
    FrameRate destFrameRate = FrameRate.getFrameRate(destFr);
    Object value = "01:00:00:00";
    Object expected = "01:00:03:18";
    assertEquals("Erro convertendo de 23.98 para 29.97 " + value, expected, srcFrameRate.convertFrameRate(value, MesureUnit.STRING, destFrameRate));
  }

  @Test
  public void convertFrameRate2997to2398() {
    String destFr = "23.98";
    String srcFr = "29.97";
    FrameRate srcFrameRate = FrameRate.getFrameRate(srcFr);
    FrameRate destFrameRate = FrameRate.getFrameRate(destFr);
    Object value = "01:00:00:00";
    Object expected = "00:59:56:09";
    assertEquals("Erro convertendo de " + srcFr + " para " + destFr, expected, srcFrameRate.convertFrameRate(value, MesureUnit.STRING, destFrameRate));
  }



  @Test
  public void convertToTimecodeArray() {
    String string = "01:02:03:04";
    FrameRate frameRate = FrameRate.getFrameRate("30.00");
    Timecode tc = frameRate.timecodeFromString(string);

    int frameNumber = 111694;
    int milliseconds = 3723133;
    int[] components = new int[] {1, 2, 3, 4};

    assertEquals("Erro convertendo de String para Timecode Array (Frame Rate)" + string, frameRate, tc.getFrameRate());
    assertEquals("Erro convertendo de String para Timecode Array (Frame Nummber)" + string, frameNumber, tc.getFrameNumber());
    assertEquals("Erro convertendo de String para Timecode Array (Milliseconds)" + string, milliseconds, tc.getMilliseconds());
    assertArrayEquals("Erro convertendo de String para Timecode Array (Components)" + string, components, tc.getComponents());
  }

  @Test
  public void convertToTimecodeObject() {
    String string = "01:02:03:04";
    FrameRate frameRate = FrameRate.getFrameRate("30.00");
    Timecode tc = frameRate.convertToTimecode(string, MesureUnit.STRING);
    
    int frameNumber = 111694;
    int milliseconds = 3723133;
    int[] components = new int[] {1, 2, 3, 4};

    assertEquals("Erro convertendo de String para Timecode Array (Frame Rate)" + string, frameRate, tc.getFrameRate());
    assertEquals("Erro convertendo de String para Timecode Array (Frame Nummber)" + string, frameNumber, tc.getFrameNumber());
    assertEquals("Erro convertendo de String para Timecode Array (Milliseconds)" + string, milliseconds, tc.getMilliseconds());
    assertArrayEquals("Erro convertendo de String para Timecode Array (Components)" + string, components, tc.getComponents());

  }

  @Test
  public void convertToTimecodeObjectFromString() {
    String string = "01:02:03:04";
    FrameRate frameRate = FrameRate.getFrameRate("30.00");
    Timecode tc = frameRate.timecodeFromString(string);

    int frameNumber = 111694;
    int milliseconds = 3723133;
    int[] components = new int[] {1, 2, 3, 4};

    assertEquals("Erro convertendo de String para Timecode Array (Frame Rate)" + string, frameRate, tc.getFrameRate());
    assertEquals("Erro convertendo de String para Timecode Array (Frame Nummber)" + string, frameNumber, tc.getFrameNumber());
    assertEquals("Erro convertendo de String para Timecode Array (Milliseconds)" + string, milliseconds, tc.getMilliseconds());
    assertArrayEquals("Erro convertendo de String para Timecode Array (Components)" + string, components, tc.getComponents());
  }

  private void convertUnits(FrameRate frameRate, Object[][] array) {
    for (Object[] var : array) {
      convertFromString(frameRate, var);
      convertFromFrameNumber(frameRate, var);
      convertFromComponents(frameRate, var);
      convertFromMilliseconds(frameRate, var);
    }
  }

  private void convertFromString(FrameRate frameRate, Object[] var) {
    String src = (String) var[0];
    assertEquals("Erro convertendo " + frameRate.getName() + " de String para Frame Number " + src, var[1], frameRate.convertUnit(src, FrameRate.MesureUnit.STRING, FrameRate.MesureUnit.FRAME_NUMBER));
    int milis = (int) frameRate.convertUnit(src, FrameRate.MesureUnit.STRING, FrameRate.MesureUnit.MILLISECONDS);
    assertTrue("Erro convertendo " + frameRate.getName() + " de String para Milisegundos " + src + ". Valor Esperado >= " + ((int)var[3] - 1) + " valor obtido: " + milis, milis >= (int)var[3] - 1);
    assertTrue("Erro convertendo " + frameRate.getName() + " de String para Milisegundos " + src + ". Valor Esperado <= " + ((int)var[3] + 1) + " valor obtido: " + milis, milis <= (int)var[3] + 1);
    assertArrayEquals("Erro convertendo " + frameRate.getName() + " de String para Componentes " + src,(int[]) var[2], (int[]) frameRate.convertUnit(src, FrameRate.MesureUnit.STRING, FrameRate.MesureUnit.COMPONENTS));
  }

  private void convertFromFrameNumber(FrameRate frameRate, Object[] var) {
    int src = (int) var[1];
    assertEquals("Erro convertendo " + frameRate.getName() + " de Frame Number para String " + src, var[0], frameRate.convertUnit(src, FrameRate.MesureUnit.FRAME_NUMBER, FrameRate.MesureUnit.STRING));
    int milis = (int) frameRate.convertUnit(src, FrameRate.MesureUnit.FRAME_NUMBER, FrameRate.MesureUnit.MILLISECONDS);
    assertTrue("Erro convertendo " + frameRate.getName() + " de Frame Number para Milisegundos " + src + ". Valor Esperado >= " + ((int)var[3] - 1) + " valor obtido: " + milis, milis >= (int)var[3] - 1);
    assertTrue("Erro convertendo " + frameRate.getName() + " de Frame Number para Milisegundos " + src + ". Valor Esperado <= " + ((int)var[3] + 1) + " valor obtido: " + milis, milis <= (int)var[3] + 1);
    assertArrayEquals("Erro convertendo " + frameRate.getName() + " de Frame Number para Componentes " + src,(int[]) var[2], (int[]) frameRate.convertUnit(src, FrameRate.MesureUnit.FRAME_NUMBER, FrameRate.MesureUnit.COMPONENTS));
  }

  private void convertFromComponents(FrameRate frameRate, Object[] var) {
    int[] src = (int[]) var[2];
    assertEquals("Erro convertendo " + frameRate.getName() + " de Componentes para String " + Arrays.toString(src), var[0], frameRate.convertUnit(src, FrameRate.MesureUnit.COMPONENTS, FrameRate.MesureUnit.STRING));
    int milis = (int) frameRate.convertUnit(src, FrameRate.MesureUnit.COMPONENTS, FrameRate.MesureUnit.MILLISECONDS);
    assertTrue("Erro convertendo " + frameRate.getName() + " de Componentes para Milisegundos " + Arrays.toString(src) + ". Valor Esperado >= " + ((int)var[3] - 1) + " valor obtido: " + milis, milis >= (int)var[3] - 1);
    assertTrue("Erro convertendo " + frameRate.getName() + " de Componentes para Milisegundos " + Arrays.toString(src) + ". Valor Esperado <= " + ((int)var[3] + 1) + " valor obtido: " + milis, milis <= (int)var[3] + 1);
    assertEquals("Erro convertendo " + frameRate.getName() + " de Componentes para Frame Number " + Arrays.toString(src), var[1], (int) frameRate.convertUnit(src, FrameRate.MesureUnit.COMPONENTS, FrameRate.MesureUnit.FRAME_NUMBER));
  }

  private void convertFromMilliseconds(FrameRate frameRate, Object[] var) {
    int src = (int) var[3];
    assertEquals("Erro convertendo " + frameRate.getName() + " de Milisegundos para String " + src, var[0], frameRate.convertUnit(src, FrameRate.MesureUnit.MILLISECONDS, FrameRate.MesureUnit.STRING));
    assertArrayEquals("Erro convertendo " + frameRate.getName() + " de Milisegundos para Componentes " + src, (int[])var[2], (int[]) frameRate.convertUnit(src, FrameRate.MesureUnit.MILLISECONDS, FrameRate.MesureUnit.COMPONENTS));
    assertEquals("Erro convertendo " + frameRate.getName() + " de Milisegundos para Frame Number " + src, var[1], (int) frameRate.convertUnit(src, FrameRate.MesureUnit.MILLISECONDS, FrameRate.MesureUnit.FRAME_NUMBER));
  }
  

}